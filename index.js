// console.log("Hi")

// a document is your webpage
console.log(document)

//targeting the first name input field
document.querySelector("#text-firstname");


//alternatively
// document.getElementByid('text-firstname');

//for other types
	// document.getElementByClassName('class-name')
	// document.getElementByTagName('tag-name')

const textFirstName = document.querySelector('#text-firstname');

const textLastName = document.querySelector('#text-lastname');

const spanFullName = document.querySelector('#span-fullname');


textFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = textFirstName.value;
});

textLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = `${textFirstName.value} ${textLastName.value}`
})


textFirstName.addEventListener('keyup', (e) => {
	console.log(e.target);
	console.log(e.target.value);
})